package com.example.semmes.myapplication.socialNetworks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by SemmEs on 01.07.2016.
 */
public class NetwUrlDownl extends AsyncTask<Void, Integer, Boolean> {

    private String LogTag = "Logi";
    private PowerManager.WakeLock mWakeLock;
    private Activity activity;
    private Network network;
    private ProgressDialog mProgressDialog;
    private String url;
    private String message;

    public NetwUrlDownl(Activity activity, Network network, ProgressDialog mProgressDialog, String url, String message) {
        this.activity = activity;
        this.network = network;
        this.mProgressDialog = mProgressDialog;
        this.url = url;
        this.message = message;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (!network.isFirstEntry()) {
            PowerManager pm = (PowerManager) activity.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
            mWakeLock.acquire();
            mProgressDialog.show();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        if (!network.isLogged()) {
            network.setFirstEntry(true);
            network.makeAuthorization(activity);
        } else {
            try {
                network.makePost(activity, url, message);
                return true;
            } catch (IOException e) {
                Log.d(LogTag, "ошибка в makepost " );
                return false;
            }
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean donwload) {
        super.onPostExecute(donwload);
        if (!network.isFirstEntry()) {
            mWakeLock.release();
            mProgressDialog.dismiss();
            if (donwload) {
                Toast.makeText(activity, "Download success", Toast.LENGTH_LONG).show();
            } else
                Toast.makeText(activity, "Download error", Toast.LENGTH_SHORT).show();
        }
    }

}
