package com.example.semmes.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.semmes.myapplication.socialNetworks.FBNetwork;
import com.example.semmes.myapplication.socialNetworks.NetwUrlDownl;
import com.example.semmes.myapplication.socialNetworks.Network;
import com.example.semmes.myapplication.socialNetworks.TWNetwork;
import com.example.semmes.myapplication.socialNetworks.VKNetwork;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;

import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.widget.ShareDialog;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiLink;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKAttachments;
import com.vk.sdk.api.model.VKPhotoArray;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private String LogTag = "Logi";

    private Button postVkUrl;
    private Button postFbUrl;
    private Button postTwUrl;
    private Button test;

    private String addr = "https://pp.vk.me/c630525/v630525676/37be8/lCJovKi9gQQ.jpg";
    private EditText edt;

    private VKNetwork vkNetwork;
    private FBNetwork fbNetwork;
    private TWNetwork twNetwork;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//delete file
        String path = Environment.getExternalStorageDirectory().toString();
        File file = new File(path + "/picturesForTwitter");
        if (file.exists()) {
            for (File item : file.listFiles())
                Log.d(LogTag, item.delete() + "  delete");
        }

//progress dialog
        mProgressDialog = new ProgressDialog(MainActivity.this);
        mProgressDialog.setMessage("Downloading");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(true);
//classes for networks
        vkNetwork = new VKNetwork(getApplicationContext());

        fbNetwork = new FBNetwork(getApplicationContext(), MainActivity.this);
        fbNetwork.addPermission(Arrays.asList("publish_actions"));

        twNetwork = new TWNetwork(MainActivity.this);
// pre checking of aut
        preCheckingOfAut();
//edit and btns
        edt = (EditText) findViewById(R.id.editText);

        postVkUrl = (Button) findViewById(R.id.postVkUrl);
        postFbUrl = (Button) findViewById(R.id.postFbUrl);
        postTwUrl = (Button) findViewById(R.id.postTwUrl);
        test = (Button) findViewById(R.id.button);

        postVkUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new NetwUrlDownl(MainActivity.this, vkNetwork, mProgressDialog, addr, "Hello").execute();
            }
        });

        postFbUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new NetwUrlDownl(MainActivity.this, fbNetwork, mProgressDialog, addr, "Hello").execute();
            }
        });

        postTwUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(LogTag, "click");
                new NetwUrlDownl(MainActivity.this, twNetwork, mProgressDialog, addr, "Hello").execute();
            }
        });

        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* List<Bitmap> b = new ArrayList<Bitmap>();
                b.add(BitmapFactory.decodeResource(getResources(), R.drawable.test));
                b.add(BitmapFactory.decodeResource(getResources(), R.drawable.test1));
                Log.d(LogTag,"hetre");
                twNetwork.makePost(MainActivity.this,b,"nice");*/

                vkNetwork.makePost(MainActivity.this,"nice");
            }
        });
    }

    public void preCheckingOfAut() {
        if (vkNetwork.isLogged()) vkNetwork.setFirstEntry(false);
        else vkNetwork.setFirstEntry(true);

        if (fbNetwork.isLogged()) fbNetwork.setFirstEntry(false);
        else fbNetwork.setFirstEntry(true);

        if (twNetwork.isLogged()) twNetwork.setFirstEntry(false);
        else twNetwork.setFirstEntry(true);
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {

        if (vkNetwork.onActivityResult(requestCode, resultCode, data)) {
            Log.d(LogTag, "vk ok");
            if (vkNetwork.isFirstEntry()) {
                vkNetwork.setFirstEntry(false);
                postVkUrl.performClick();
            }
        } else {
            if (fbNetwork.onActivityResult(requestCode, resultCode, data)) {
                Log.d(LogTag, "fb ok");
                if (fbNetwork.isFirstEntry()) {
                    fbNetwork.setFirstEntry(false);
                    postFbUrl.performClick();
                }
            } else {
                if (false) {//ok

                } else {
                    twNetwork.onActivityResult(requestCode, resultCode, data);
                    Log.d(LogTag, "tw ok");
                    if (twNetwork.isFirstEntry()) {
                        twNetwork.setFirstEntry(false);
                        postTwUrl.performClick();
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
