package com.example.semmes.myapplication.socialNetworks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.widget.ShareDialog;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by SemmEs on 28.06.2016.
 */
public class FBNetwork extends Network {

    private String LogTag = "Logi";

    private CallbackManager callbackManager;
    private LoginManager loginManager;
    private List<String> permissionNeeds;
    private ShareDialog shareDialog;

    public FBNetwork(Context context, Activity activity) {
        FacebookSdk.sdkInitialize(context);
        AppEventsLogger.activateApp(context);


        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(activity);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.d(LogTag, "register callback success");
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });
        loginManager = LoginManager.getInstance();
    }

    @Override
    public void makeAuthorization(Activity activity) {
        //this loginManager helps you eliminate adding a LoginButton to your UI
        Log.d(LogTag, "aut");
        loginManager.logInWithPublishPermissions(activity, permissionNeeds);
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(LogTag, "Success ");
            }

            @Override
            public void onCancel() {
                Log.d(LogTag, "Cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.d(LogTag, "Error");
            }
        });
    }

    @Override
    public boolean isLogged() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken == null ? false : true;
    }

    @Override
    public void makePost(final Activity activity, Bitmap bitmap, String message) {
        SharePhoto sharePhoto = new SharePhoto.Builder()
                .setBitmap(bitmap)
                .setCaption(message)
                .build();
                        /*SharePhotoContent content = new SharePhotoContent.Builder()
                                .addPhoto(photo)
                                .build();*/
        ShareContent shareContent = new ShareMediaContent.Builder()
                .addMedium(sharePhoto)
                .build();
        //shareDialog = new ShareDialog(MainActivity.this);
        shareDialog.show(shareContent);
    }

    @Override
    public void makePost(final Activity activity, String urlString, String message) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        InputStream input = new BufferedInputStream(urlConnection.getInputStream());
        Bitmap bitmap = BitmapFactory.decodeStream(input);
        this.makePost(activity, bitmap, message);
        urlConnection.disconnect();
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        return callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void addPermission(List<String> publish_actions) {
        permissionNeeds = publish_actions;
    }

}
