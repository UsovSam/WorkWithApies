package com.example.semmes.myapplication.socialNetworks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.Toast;

import com.example.semmes.myapplication.R;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKAttachments;
import com.vk.sdk.api.model.VKPhotoArray;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by SemmEs on 28.06.2016.
 */
public class VKNetwork extends Network{

    private String LogTag = "Logi";

    VKAttachments attachments;
    private String[] scope = {VKScope.MESSAGES, VKScope.FRIENDS, VKScope.WALL, VKScope.PHOTOS};
    private String message = "";

    public VKNetwork(Context context){
        VKSdk.initialize(context);
    }

    @Override
    public void makeAuthorization(Activity activity) {
        VKSdk.login(activity,scope);
    }

    @Override
    public boolean isLogged() {
        return VKSdk.isLoggedIn();
    }

    @Override
    public void makePost(final Activity activity, String urlString, String message) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        InputStream input = new BufferedInputStream(urlConnection.getInputStream());
        Bitmap bitmap = BitmapFactory.decodeStream(input);
        this.makePost(activity, bitmap, message);
        urlConnection.disconnect();
    }


    public void makePost(final Activity activity, String text) {
        this.message = text;
        Bitmap b = BitmapFactory.decodeResource(activity.getResources(), R.drawable.test);
        Bitmap b1 = BitmapFactory.decodeResource(activity.getResources(), R.drawable.test1);
        attachments = new VKAttachments();

        Log.d(LogTag,"hey " + attachments.size());
        VKRequest request = VKApi.uploadWallPhotoRequest(new VKUploadImage(b, VKImageParameters.jpgImage(0.9f)), 0, 0);
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {

                VKApiPhoto photoModel = new VKApiPhoto();//(VKPhotoArray) response.parsedModel).get(0);
                attachments.add(((VKPhotoArray) response.parsedModel).get(0));
                Log.d(LogTag,"hey " + attachments.size());
            }

            @Override
            public void onError(VKError error) {
                Toast.makeText(activity.getApplicationContext(),"Операция не прошла", Toast.LENGTH_SHORT).show();
            }
        });

        Log.d(LogTag,"hey " + attachments.size());
        VKRequest request1 = VKApi.uploadWallPhotoRequest(new VKUploadImage(b1, VKImageParameters.jpgImage(0.9f)), 0, 0);
        request1.executeAfterRequest(request,new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
               VKApiPhoto photoModel = ((VKPhotoArray) response.parsedModel).get(0);
                attachments.add(((VKPhotoArray) response.parsedModel).get(0));
                Log.d(LogTag,"hey " + attachments.size());
            }

            @Override
            public void onError(VKError error) {
                Toast.makeText(activity.getApplicationContext(),"Операция не прошла", Toast.LENGTH_SHORT).show();
            }
        });

        Log.d(LogTag,"hey " + attachments.size());
        VKRequest post = VKApi.wall().post(VKParameters.from(VKApiConst.ATTACHMENTS, attachments, VKApiConst.MESSAGE, message));
        post.executeAfterRequest(request1,new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                Toast.makeText(activity.getApplicationContext(),"Операция прошла успешно", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void makePost(final Activity activity, Bitmap bitmap, String text) {
        this.message = text;
        VKRequest request = VKApi.uploadWallPhotoRequest(new VKUploadImage(bitmap, VKImageParameters.jpgImage(0.9f)), 0, 0);
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {

                VKApiPhoto photoModel = ((VKPhotoArray) response.parsedModel).get(0);
                attachments = new VKAttachments(photoModel);
                VKRequest post = VKApi.wall().post(VKParameters.from(VKApiConst.ATTACHMENTS, attachments, VKApiConst.MESSAGE, message));
                post.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        Toast.makeText(activity.getApplicationContext(),"Операция прошла успешно", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onError(VKError error) {
                Toast.makeText(activity.getApplicationContext(),"Операция не прошла", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        return VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                Log.d(LogTag, "good vk");
            }

            @Override
            public void onError(VKError error) {
                Log.d(LogTag, "error vk");
            }
        });
    }


}
