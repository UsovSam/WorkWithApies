package com.example.semmes.myapplication.socialNetworks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import java.io.IOException;
import java.net.MalformedURLException;

/**
 * Created by SemmEs on 28.06.2016.
 */
public abstract class Network {

    private Boolean firstEntry = true;

    public abstract void makePost(final Activity activity, Bitmap bitmap, String message);

    public abstract void makeAuthorization(Activity activity);

    public abstract boolean isLogged();

    public abstract void makePost(final Activity activity, String urlString, String message) throws IOException;

    public abstract boolean onActivityResult(int requestCode, int resultCode, Intent data);

    public Boolean isFirstEntry() {
        return firstEntry;
    }

    public void setFirstEntry(Boolean firstEntry) {
        this.firstEntry = firstEntry;
    }

}
