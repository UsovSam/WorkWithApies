package com.example.semmes.myapplication.socialNetworks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.example.semmes.myapplication.R;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by SemmEs on 30.06.2016.
 */
public class TWNetwork extends Network {

    private String LogTag = "Logi";

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "0kZPsnRbTVnewfclScIdMrBSZ";
    private static final String TWITTER_SECRET = "1YTu0YFwdfjq6ztaCv9AURtaMQLwujInUK4fbFuBXDKnzkib1w";

    private Uri uri;
    TwitterAuthClient twitterAuthClient;

    public TWNetwork(Context context) {
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(context, new Twitter(authConfig));
        Twitter.getInstance();
        twitterAuthClient = new TwitterAuthClient();
        this.uri = null;
    }

    @Override
    public void makeAuthorization(final Activity activity) {
        twitterAuthClient.authorize(activity, new Callback<TwitterSession>() {

            @Override
            public void success(Result<TwitterSession> twitterSessionResult) {
                Log.d(LogTag, " suucces");
                //Toast.makeText(activity.getApplicationContext(), "success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(TwitterException e) {
                Log.d(LogTag, " failure");
                //Toast.makeText(activity.getApplicationContext(), "failure", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public boolean isLogged() {
        return Twitter.getSessionManager().getActiveSession() != null ? true : false;
    }

    public void makePost(final Activity activity, List<Bitmap> bitmapList, String message) {
        Log.d(LogTag,"hetre");
        List<Uri> uriList = new ArrayList<Uri>(bitmapList.size());
        for (int i = 0; i < bitmapList.size(); i++)
            uriList.add(getImageUri(activity, bitmapList.get(i),i));
        Log.d(LogTag,"hetre");
        TweetComposer.Builder tw = new TweetComposer.Builder(activity).text(message);
       // for(Uri uri : uriList)
            tw.image(uriList.get(0));
        Log.d(LogTag,"hetre");
        tw.show();
    }

    @Override
    public void makePost(final Activity activity, Bitmap bitmap, String message) {
        Log.d(LogTag, "make post tw");
        Uri uri = getImageUri(activity, bitmap);
        this.uri = uri;
        new TweetComposer.Builder(activity)
                .text(message)
                .image(uri)
                .show();
    }

    @Override
    public void makePost(final Activity activity, String urlString, String message) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        InputStream input = new BufferedInputStream(urlConnection.getInputStream());
        Bitmap bitmap = BitmapFactory.decodeStream(input);
        this.makePost(activity, bitmap, message);
        urlConnection.disconnect();
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        twitterAuthClient.onActivityResult(requestCode, resultCode, data);
        return true;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        try {
            Log.d(LogTag, "here");
            String path1;
            File file;
            // проверяем доступность SD // что-то не але
            if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                Log.d(LogTag, "SD-карта не доступна: " + Environment.getExternalStorageState());
                path1 = "picturesForTwitter";
                file = new File(path1);
            } else {
                path1 = Environment.getExternalStorageDirectory().toString();
                File mydir = new File(path1 + "/picturesForTwitter");
                mydir.mkdir();
                file = new File(mydir, "forTweets.jpg");
            }

            FileOutputStream bytes = new FileOutputStream(file);
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            bytes.flush();
            bytes.close();

            return Uri.parse(file.getAbsolutePath());

        } catch (Exception e) {
            return null;
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage, int i) {
        try {
            Log.d(LogTag, "here");
            String path1;
            File file;
            // проверяем доступность SD // что-то не але
            if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                Log.d(LogTag, "SD-карта не доступна: " + Environment.getExternalStorageState());
                path1 = "picturesForTwitter";
                file = new File(path1);
            } else {
                path1 = Environment.getExternalStorageDirectory().toString();
                File mydir = new File(path1 + "/picturesForTwitter");
                mydir.mkdir();
                file = new File(mydir, "forTweets" +i+".jpg");
            }

            FileOutputStream bytes = new FileOutputStream(file);
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            bytes.flush();
            bytes.close();

            return Uri.parse(file.getAbsolutePath());

        } catch (Exception e) {
            return null;
        }
    }
}
